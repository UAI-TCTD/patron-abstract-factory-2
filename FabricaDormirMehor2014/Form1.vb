﻿Public Class Form1

    Private Sub btnCrearSommierQueen_Click(sender As Object, e As EventArgs) Handles btnCrearSommierQueen.Click
        Try
            Dim factoria As FactoriaAbstracta = New FactoriaSommierQueen

            LineaDeProduccion.ObtenerInstancia().CrearPedido(factoria)

            lstPendientes.DataSource = Nothing
            lstPendientes.DataSource = LineaDeProduccion.ObtenerInstancia().ObtenerListaPendientes()

        Catch ex As Exception

        End Try

    End Sub

    Private Sub btnCrearClasicoQueen_Click(sender As Object, e As EventArgs) Handles btnCrearClasicoQueen.Click
        Try
            Dim factoria As FactoriaAbstracta = New FactoriaClasicaQueen

            LineaDeProduccion.ObtenerInstancia().CrearPedido(factoria)

            lstPendientes.DataSource = Nothing
            lstPendientes.DataSource = LineaDeProduccion.ObtenerInstancia().ObtenerListaPendientes()

        Catch ex As Exception

        End Try
    End Sub

    Private Sub btnTerminar_Click(sender As Object, e As EventArgs) Handles btnTerminar.Click
        LineaDeProduccion.ObtenerInstancia().TerminarPedido()

        lstPendientes.DataSource = Nothing
        lstPendientes.DataSource = LineaDeProduccion.ObtenerInstancia().ObtenerListaPendientes()

        lstTerminados.DataSource = Nothing
        lstTerminados.DataSource = LineaDeProduccion.ObtenerInstancia().ObtenerListaTerminados()

    End Sub

    Private Sub btnEntregar_Click(sender As Object, e As EventArgs) Handles btnEntregar.Click
        LineaDeProduccion.ObtenerInstancia().EntregarPedido()

        lstTerminados.DataSource = Nothing
        lstTerminados.DataSource = LineaDeProduccion.ObtenerInstancia().ObtenerListaTerminados()

    End Sub
End Class
