﻿Public Class LineaDeProduccion

    'implementacion del Singleton
    Private Shared ReadOnly instancia As LineaDeProduccion = New LineaDeProduccion()
    Private Sub New()
    End Sub
    Public Shared Function ObtenerInstancia() As LineaDeProduccion
        Return instancia
    End Function


    Private listaPendientes As List(Of Producto) = New List(Of Producto)
    Private listaTerminados As List(Of Producto) = New List(Of Producto)

    Public Sub CrearPedido(factoria As FactoriaAbstracta)
        Dim p As Producto = New Producto

        p.Colchon = factoria.CrearColchon()
        p.BaseDeCama = factoria.CrearBaseDeCama()

        listaPendientes.Add(p)
    End Sub

    Public Sub TerminarPedido()
        Me.listaTerminados.Add(Me.listaPendientes(0))
        Me.listaPendientes.RemoveAt(0)
    End Sub

    Public Sub EntregarPedido()
        Me.listaTerminados.RemoveAt(0)
    End Sub

    Public Function ObtenerListaPendientes() As Producto()
        Return Me.listaPendientes.ToArray()
    End Function

    Public Function ObtenerListaTerminados() As Producto()
        Return Me.listaTerminados.ToArray()
    End Function

End Class
