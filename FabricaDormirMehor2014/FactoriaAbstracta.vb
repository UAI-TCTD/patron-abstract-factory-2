﻿Public Interface IFactoriaAbstracta
    Function CrearBaseDeCama() As BaseDeCama
    Function CrearColchon() As Colchon
End Interface

Public MustInherit Class FactoriaAbstracta

    Public MustOverride Function CrearBaseDeCama() As BaseDeCama

    Public MustOverride Function CrearColchon() As Colchon

End Class

Public Class FactoriaSommierQueen
    Inherits FactoriaAbstracta

    Public Overrides Function CrearBaseDeCama() As BaseDeCama
        Dim b As BaseSommier = New BaseSommier()
        b.Ancho = 160
        b.Largo = 190
        b.Descripcion = "Base Sommier Queen"
        Return b
    End Function

    Public Overrides Function CrearColchon() As Colchon
        Dim c As ColchonResortes = New ColchonResortes()
        c.Alto = 30
        c.Ancho = 160
        c.Largo = 190
        c.Descripcion = "Colchon Sommier Queen"
        Return c
    End Function
End Class

Public Class FactoriaClasicaQueen
    Inherits FactoriaAbstracta

    Public Overrides Function CrearBaseDeCama() As BaseDeCama
        Dim b As BaseMadera = New BaseMadera()
        b.Ancho = 160
        b.Largo = 190
        b.Descripcion = "Base Madera Queen"
        Return b
    End Function

    Public Overrides Function CrearColchon() As Colchon
        Dim c As ColchonEspuma = New ColchonEspuma()
        c.Alto = 30
        c.Ancho = 160
        c.Largo = 190
        c.Descripcion = "Colchon Espuma Queen"
        Return c
    End Function
End Class