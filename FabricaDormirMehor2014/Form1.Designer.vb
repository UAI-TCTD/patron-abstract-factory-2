﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnCrearSommierQueen = New System.Windows.Forms.Button()
        Me.btnCrearClasicoQueen = New System.Windows.Forms.Button()
        Me.lstPendientes = New System.Windows.Forms.ListBox()
        Me.lstTerminados = New System.Windows.Forms.ListBox()
        Me.btnTerminar = New System.Windows.Forms.Button()
        Me.btnEntregar = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'btnCrearSommierQueen
        '
        Me.btnCrearSommierQueen.Location = New System.Drawing.Point(15, 15)
        Me.btnCrearSommierQueen.Margin = New System.Windows.Forms.Padding(6, 6, 6, 6)
        Me.btnCrearSommierQueen.Name = "btnCrearSommierQueen"
        Me.btnCrearSommierQueen.Size = New System.Drawing.Size(138, 81)
        Me.btnCrearSommierQueen.TabIndex = 0
        Me.btnCrearSommierQueen.Text = "Sommier Queen"
        Me.btnCrearSommierQueen.UseVisualStyleBackColor = True
        '
        'btnCrearClasicoQueen
        '
        Me.btnCrearClasicoQueen.Location = New System.Drawing.Point(171, 18)
        Me.btnCrearClasicoQueen.Name = "btnCrearClasicoQueen"
        Me.btnCrearClasicoQueen.Size = New System.Drawing.Size(138, 78)
        Me.btnCrearClasicoQueen.TabIndex = 1
        Me.btnCrearClasicoQueen.Text = "Clasico Queen"
        Me.btnCrearClasicoQueen.UseVisualStyleBackColor = True
        '
        'lstPendientes
        '
        Me.lstPendientes.FormattingEnabled = True
        Me.lstPendientes.ItemHeight = 24
        Me.lstPendientes.Location = New System.Drawing.Point(23, 185)
        Me.lstPendientes.Name = "lstPendientes"
        Me.lstPendientes.Size = New System.Drawing.Size(480, 100)
        Me.lstPendientes.TabIndex = 2
        '
        'lstTerminados
        '
        Me.lstTerminados.FormattingEnabled = True
        Me.lstTerminados.ItemHeight = 24
        Me.lstTerminados.Location = New System.Drawing.Point(23, 345)
        Me.lstTerminados.Name = "lstTerminados"
        Me.lstTerminados.Size = New System.Drawing.Size(480, 76)
        Me.lstTerminados.TabIndex = 3
        '
        'btnTerminar
        '
        Me.btnTerminar.Location = New System.Drawing.Point(23, 291)
        Me.btnTerminar.Name = "btnTerminar"
        Me.btnTerminar.Size = New System.Drawing.Size(239, 38)
        Me.btnTerminar.TabIndex = 4
        Me.btnTerminar.Text = "Terminar"
        Me.btnTerminar.UseVisualStyleBackColor = True
        '
        'btnEntregar
        '
        Me.btnEntregar.Location = New System.Drawing.Point(23, 427)
        Me.btnEntregar.Name = "btnEntregar"
        Me.btnEntregar.Size = New System.Drawing.Size(239, 37)
        Me.btnEntregar.TabIndex = 5
        Me.btnEntregar.Text = "Entregar"
        Me.btnEntregar.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(11.0!, 24.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(521, 499)
        Me.Controls.Add(Me.btnEntregar)
        Me.Controls.Add(Me.btnTerminar)
        Me.Controls.Add(Me.lstTerminados)
        Me.Controls.Add(Me.lstPendientes)
        Me.Controls.Add(Me.btnCrearClasicoQueen)
        Me.Controls.Add(Me.btnCrearSommierQueen)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Margin = New System.Windows.Forms.Padding(6, 6, 6, 6)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnCrearSommierQueen As System.Windows.Forms.Button
    Friend WithEvents btnCrearClasicoQueen As System.Windows.Forms.Button
    Friend WithEvents lstPendientes As System.Windows.Forms.ListBox
    Friend WithEvents lstTerminados As System.Windows.Forms.ListBox
    Friend WithEvents btnTerminar As System.Windows.Forms.Button
    Friend WithEvents btnEntregar As System.Windows.Forms.Button

End Class
