﻿Public MustInherit Class BaseDeCama
 
    Private _ancho As Integer
    Public Property Ancho() As Integer
        Get
            Return _ancho
        End Get
        Set(ByVal value As Integer)
            _ancho = value
        End Set
    End Property

    Private _largo As Integer
    Public Property Largo() As Integer
        Get
            Return _largo
        End Get
        Set(ByVal value As Integer)
            _largo = value
        End Set
    End Property

    Private _descripcion As String
    Public Property Descripcion() As String
        Get
            Return _descripcion
        End Get
        Set(ByVal value As String)
            _descripcion = value
        End Set
    End Property
End Class

Public Class BaseMadera
    Inherits BaseDeCama

End Class

Public Class BaseSommier
    Inherits BaseDeCama

End Class