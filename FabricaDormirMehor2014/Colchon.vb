﻿Public MustInherit Class Colchon

    Private _alto As Integer
    Public Property Alto() As Integer
        Get
            Return _alto
        End Get
        Set(ByVal value As Integer)
            _alto = value
        End Set
    End Property

    Private _ancho As Integer
    Public Property Ancho() As Integer
        Get
            Return _ancho
        End Get
        Set(ByVal value As Integer)
            _ancho = value
        End Set
    End Property

    Private _largo As Integer
    Public Property Largo() As Integer
        Get
            Return _largo
        End Get
        Set(ByVal value As Integer)
            _largo = value
        End Set
    End Property

    Private _descripcion As String
    Public Property Descripcion() As String
        Get
            Return _descripcion
        End Get
        Set(ByVal value As String)
            _descripcion = value
        End Set
    End Property

End Class

Public Class ColchonEspuma
    Inherits Colchon

End Class

Public Class ColchonResortes
    Inherits Colchon

    Private _cantResortes As Byte
    Public Property CantidadDeResortes() As Byte
        Get
            Return _cantResortes
        End Get
        Set(ByVal value As Byte)
            _cantResortes = value
        End Set
    End Property

End Class
