﻿Public Class Producto
    Private _colchon As Colchon
    Public Property Colchon() As Colchon
        Get
            Return _colchon
        End Get
        Set(ByVal value As Colchon)
            _colchon = value
        End Set
    End Property
    Private _baseDeCama As BaseDeCama
    Public Property BaseDeCama() As BaseDeCama
        Get
            Return _baseDeCama
        End Get
        Set(ByVal value As BaseDeCama)
            _baseDeCama = value
        End Set
    End Property

    Public Overrides Function ToString() As String
        Return Me.Colchon.Descripcion & " " & Me.BaseDeCama.Descripcion
    End Function

End Class
